const cards = require("./cards.json");
// console.log(cards)

let cardsInfo = (listID, cards) => {
    let cardsID = Object.keys(cards);
    let fetchedCards = cardsID.reduce((acc, curr) =>{
        if(curr === listID){
            acc.push(cards[curr]);
        }
        return acc;
    }, [])
    return fetchedCards;
}

let mainFunc = (callback, listID) => {
    setTimeout(() =>{
        console.log(callback(listID, cards))
    }, 2000)
}

module.exports = {mainFunc, cardsInfo}