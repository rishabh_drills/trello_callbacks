const boards = require("./boards.json");
 //console.log(boards);

let boardInfo = (boardID, boards) => {
    let info = boards.reduce((acc, curr) => {
        if(curr["id"] == boardID){
            acc.push(curr);
        }
        return acc;
    }, []);
    return info;
}
// console.log(boardInfo("mcu453ed", boards))

let mainFunc = (callback, boardID) => {
    
    setTimeout(() =>{
        console.log(callback(boardID, boards));        
    }, 2000)

    
}
// console.log(mainFunc())

module.exports = {mainFunc, boardInfo};
