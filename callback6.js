const boards = require("./boards.json");
const lists = require("./lists.json");
const cards = require("./cards.json");

const obj1 = require("./callback1");
const boardInfo = obj1.boardInfo;
const mainFunc1 = obj1.mainFunc;

const obj2 = require("./callback2");
const listed = obj2.listed;
const mainFunc2 = obj2.mainFunc;

const obj3 = require("./callback3");
const cardsInfo = obj3.cardsInfo;
const mainFunc3 = obj3.mainFunc;

const getInformation = () => {
    let getName = boards.reduce((acc, curr) => {
        if(curr["name"] === "Thanos"){
            acc = curr["id"];
        }
        return acc;
    },[])
    setTimeout(() => {
        console.log(boardInfo(getName, boards))

        setTimeout(() => {
            console.log(listed(getName, lists))

            setTimeout(() => {
                let result = Object.entries(lists).reduce((accumulator, currVal) => {
                    // console.log(currVal[1])
                    for(let i = 0; i < currVal[1].length; i++){
                        
                            accumulator.push(currVal[1][i]["id"]);
                        
                    }
                    return accumulator;                 
                },[]) 
                // console.log(result)

                for(let i = 0; i < result.length; i++){
                    console.log(cardsInfo(result[0], cards))
                }     
                
                // console.log(cardsInfo(result[1], cards))

            }, 2000)
        }, 3000)
    }, 4000)
}

module.exports = getInformation;